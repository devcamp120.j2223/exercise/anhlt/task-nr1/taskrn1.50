//  Khai báo thư viện express
const express = require('express');

// Khai báo mongo
const mongoose = require('mongoose');

// IMPORT ROUTER

const diceHistoryRouter = require('./app/router/diceHistoryRouter');

// Khai báo thư viện path
const path = require('path');
const userRouter = require('./app/router/userRouter');

//  Khai báo app nodeJS
const app = new express();

//Khai báo middleware json
app.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

//  Khai báo cổng nodeJS
const port = 8000;

// Sử dụng Mongo
mongoose.connect("mongodb://localhost:27017/CRUD_LuckyDice", (error) => {
    if (error) {
        throw error;
    }
    console.log("Connect MongoDB successfully!!!")
})

// KHAI BÁO VIEWS
app.use(express.static(path.join(__dirname + '/views')));

// Khai báo API
app.get('/', (request, response) => {
    response.sendFile(path.join(__dirname + '/views/Lucky-Dice.html'))
})


// SỬ DỤNG ROUTER
app.use('/', diceHistoryRouter);
app.use('/', userRouter)

//  CHẠY CỔNG NODEJS
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})